var http = require('http');
var fs = require('graceful-fs');
var path = require('path');
var parseManifest = require("parse-appcache-manifest")
var mkpath = require('mkpath');
var _ = require ('underscore')
var ProgressBar = require('progress');

require('date-utils');

var config = {
  url : "http://ingame-v01-ue1.firefallthegame.com",
  manifest : "/cache.manifest",
  localpath : ".",
  loglevel:"log"
}

var logger = require('./logger')
logger.debugLevel = config.loglevel;

try{
  var file = fs.openSync('./config.json', 'r')
  fs.close(file)
  config = require('./config.json')
}catch(e){
  var configFile = fs.writeFileSync('./config.json', JSON.stringify(config, null, 2))
  logger.warn('No config file found. Default file has been created')
}


var url = config.url

var dot = config.localpath
var manifest = config.manifest
var manifestL = "."+manifest

var local;
var remote;

var download = function(url, dest, cb) {
  var file = fs.createWriteStream(dest);
  var request = http.get(url, function(response) {
    response.pipe(file);
    file.on('finish', function() {
      file.close(cb);  // close() is async, call cb after close completes.
    });
  }).on('error', function(err) { // Handle errors
    fs.unlink(dest); // Delete the file async. (But we don't check the result)
    if (cb) cb(err.message);
  });
};

var httpStream = function(url, cb){
  var err;
  var body = "";
  http.get(url, function(res) {
    res.setEncoding('utf8');
    res.on("data", function(chunk) {
      body += chunk;
    });
    res.on('end', function() {
      cb(err, body);
    });
  }).on('error', function(err) {
    console.log("Got error: " + err.message);
    cb(err)
  });
}

var writeDiffFile = function(diff, prefix){
  mkpath.sync('./diff-logs');
  var prefix = prefix || "diff";
  var d = new Date();
  var ds = d.toFormat("YYYY.MM.DD-HH24.MI")
  var filename = './diff-logs/' + prefix + '-' +  ds  + '.txt';
  
  var logfile = fs.writeFile(filename, JSON.stringify(diff, null, 2), function (err) {
    if (err) logger.error(err);
  });
}

var traverseFileSystem = function (currentPath, list) {
  
  list = (typeof list === "undefined") ? [] : list;
  
  var files = fs.readdirSync(currentPath);
  for (var i in files) {
     var currentFile = currentPath + '/' + files[i];
     
     var stats = fs.statSync(currentFile);
     if (stats.isFile() && stats.size > 0) {
       list.push("/"+currentFile)
     }
     else if (stats.isDirectory()) {
        list = traverseFileSystem(currentFile, list);
     }
    
   }
   return list
};

/***************************************************************/
mkpath.sync('./assets');

local = traverseFileSystem('assets');
logger.info("indexed local files")

httpStream(url+manifest, function(err, data){
  remote = parseManifest(data).cache;
  logger.info("indexed remote files")
  
  var diff = _.difference(remote, local)
  
  //fix for Aero Matics fonts (random folder capitalization)
  diff.forEach(function(e, i){
     diff[i] = path.dirname(e).toLowerCase() + "/" + path.basename(e)
  })
  diff = _.difference(diff, local)

  logger.warn(diff.length + " new files")
  
  if(diff.length > 0){
    writeDiffFile(diff)
  }
  
  var bar = new ProgressBar(':percent [:bar] :title', { total: diff.length, width : 10, clear : true, complete: '=', incomplete: ' '});
  
  diff.forEach(function(e){
    
    mkpath.sync(path.dirname(dot+e));
    download(url+e, dot+e, function(err){
      if(err){
        logger.error('\r\nerror downloading file: ' + e)
        logger.error(err)
      }
      //to avoid getting empty files into the next diff
      var stats = fs.stat(dot+e, function(err, stats){
        if(err) logger.error(err)
        if(stats.size == 0){
          fs.writeFile(dot+e, "this file is empty on the remote", function (err) {
            if (err) logger.error(err);
          });
        }
      })
      
      var stitle = e
      stitle = stitle.substring(0, 39) + "..." + stitle.substring(stitle.length - 20)
      bar.tick({title : stitle})
      if (bar.complete) {
        logger.info("done")
      }
    })
    
  
  });

})
logger.info("building remote index")
