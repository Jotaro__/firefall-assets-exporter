var fs = require('fs')
var pak = require('./package.json');

var file = './build/firefall-assets-exporter.jxp';

fs.readFile(file, function (err, data) {
  if (err) throw err;
  var jxp = JSON.parse(data);
  
  jxp.version = pak.version
  
  fs.writeFile(file, JSON.stringify(jxp, null, 2), function(err){
    if(err) throw err;
  })
  
});